require 'rails_helper.rb'

feature 'Creating comment' do
  background do
    user = create(:user)
    post = create(:post, user_id: user.id)
    @post2 = create(:post, user_id: user.id)

    sign_in_with(user)
    visit '/'
  end

  scenario 'adds comment to post', :js => true do
    fill_in "comment_content_#{@post2.id}", with: "nice_comment_eh\n"
    expect(page).to have_css("div\#comments_#{@post2.id}", text: 'nice_comment_eh')
  end
end
