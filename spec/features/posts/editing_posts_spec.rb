require 'rails_helper'

feature 'can edit a post' do
  background do
    user = create(:user)
    user_two = create(:user, email: 'ex2@ex.com',
                             user_name: 'User222',
                             id: user.id + 1)

    @post = create(:post, user_id: user.id)
    @post_two = create(:post, user_id: user_two.id)

    sign_in_with(user)
    visit '/'
  end

  scenario 'can edit and update post as the owner' do
    find(:xpath, "//a[contains(@href,\"posts/#{@post.id}\")]").click
    expect(page).to have_content('Edit')
    click_link 'Edit'

    fill_in 'post_caption', with: 'This is updated text.'
    click_button 'Update Post'
    expect(page).to have_content('Successfully updated post.')
    expect(page).to have_content('This is updated text.')
  end

  scenario 'cannot edit post of another user' do
    find(:xpath, "//a[contains(@href,\"posts/#{@post_two.id}\")]").click
    expect(page).to_not have_content('Edit')
  end

  scenario "can't edit post that does not belong to you via direct url path" do
    visit "/posts/#{@post_two.id}/edit"
    expect(page).to_not have_content('Edit')
  end

  scenario 'fails to update post without an image' do
    find(:xpath, "//a[contains(@href,\"posts/#{@post.id}\")]").click
    click_link 'Edit'
    attach_file('post_image', 'spec/files/coffee.html')
    click_button 'Update Post'
    expect(page).to have_content('Failed to update post.')
  end
end
