require 'rails_helper.rb'

feature 'can view individual posts' do
  background do
    @user = create(:user)
    sign_in_with(@user)
  end

  scenario 'clicking on image in index view goes to img show page' do
    post = create(:post, user_id: @user.id)
    visit '/'
    find(:xpath, "//a[contains(@href,\"posts/#{post.id}\")]").click

    expect(page.current_path).to eq(post_path(post))
  end
end
