#Instagram clone

[Website link][weburl]

[weburl]: https://instantpic.herokuapp.com/

This is an Instagram clone built with rails and some JavaScript.

Mainly I'm building this app to test out a few gems and setups that I haven't used before.

###Namely:
* Simple form
* paperclip
* devise
* active admin.

Also, testing Amazon S3 setup to store images.

Planning to move hosting from Heroku to Amazon to get familiar with capistrano.

Credit to devwalks.com for providing a great guide.
