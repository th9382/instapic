class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :own_post, only: [:edit, :update, :destroy]

  def index
    @posts = Post.all.order(created_at: :desc).page params[:page]
  end

  def new
    @post = current_user.posts.build
  end

  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      flash[:success] = 'Successfully submitted post.'
      redirect_to @post
    else
      flash.now[:error] = 'Failed to create post.'
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @post.update(post_params)
      flash[:success] = 'Successfully updated post.'
      redirect_to @post
    else
      flash.now[:error] = 'Failed to update post.'
      render :edit
    end
  end

  def destroy
    @post.destroy
    flash[:success] = 'Post deleted.'
    redirect_to root_url
  end

  private

  def post_params
    params.require(:post).permit(:image, :caption)
  end

  def set_post
    @post = Post.find(params[:id])
  end

  def own_post
    unless current_user == @post.user
      flash[:error] = 'Can only edit your own posts!'
      redirect_to root_url
    end
  end
end
